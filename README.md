# Berci's Spotr.ai assignment

This is the folder for my submission for the Spotr.ai assignment. The assignment's description can be found [here](https://spotr-ai.slite.com/api/s/note/Xk8KzTYcGpnQtzEpbovvtf/Spotr-ai-Full-Stack-Assignment).

## Setup

To run the app, first install the dependencies in both the main folder and the ./server folder, then run the application.

`npm i`  
`cd server`  
`npm i`  
`npm run dev`

This will start both the client and the backend for retrieving data.
It is important that you run `npm run dev` in the server folder, otherwise only the client gets run.
