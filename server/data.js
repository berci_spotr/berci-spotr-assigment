module.exports = [
  {
    source: "streetview",
    date: "02-02-2021",
    images: [
      {
        url: "http://localhost:5000/streetview/IMG_20210617_111603.jpg",
      },
      {
        url: "http://localhost:5000/streetview/IMG_20210617_111619.jpg",
      },
      {
        url: "http://localhost:5000/streetview/IMG_20210617_111714.jpg",
      },
      {
        url: "http://localhost:5000/streetview/IMG_20210617_111817.jpg",
      },
      {
        url: "http://localhost:5000/streetview/IMG_20210617_111948.jpg",
      },
      {
        url: "http://localhost:5000/streetview/IMG_20210617_112042.jpg",
      },
      {
        url: "http://localhost:5000/streetview/IMG_20210617_112214.jpg",
      },
      {
        url: "http://localhost:5000/streetview/IMG_20210617_112232.jpg",
      },
      {
        url: "http://localhost:5000/streetview/IMG_20210617_112338.jpg",
      },
      {
        url: "http://localhost:5000/streetview/IMG_20210617_112356.jpg",
      },
      {
        url: "http://localhost:5000/streetview/IMG_20210617_112453.jpg",
      },
      {
        url: "http://localhost:5000/streetview/IMG_20210617_112600.jpg",
      },
      {
        url: "http://localhost:5000/streetview/IMG_20210617_112736.jpg",
      },
      {
        url: "http://localhost:5000/streetview/IMG_20210617_112817.jpg",
      },
      {
        url: "http://localhost:5000/streetview/IMG_20210617_113600.jpg",
      },
    ],
  },
  {
    source: "inspector",
    date: "03-04-2021",
    images: [
      {
        url: "http://localhost:5000/inspector/IMG_20210617_114243.jpg",
      },
      {
        url: "http://localhost:5000/inspector/IMG_20210617_114244.jpg",
      },
      {
        url: "http://localhost:5000/inspector/IMG_20210617_114408.jpg",
      },
      {
        url: "http://localhost:5000/inspector/IMG_20210617_114445.jpg",
      },
      {
        url: "http://localhost:5000/inspector/IMG_20210617_114701.jpg",
      },
      {
        url: "http://localhost:5000/inspector/IMG_20210617_114722.jpg",
      },
      {
        url: "http://localhost:5000/inspector/IMG_20210617_114747.jpg",
      },
      {
        url: "http://localhost:5000/inspector/IMG_20210617_114829.jpg",
      },
      {
        url: "http://localhost:5000/inspector/IMG_20210617_115155.jpg",
      },
      {
        url: "http://localhost:5000/inspector/IMG_20210617_124348.jpg",
      },
      {
        url: "http://localhost:5000/inspector/IMG_20210617_124406.jpg",
      },
      {
        url: "http://localhost:5000/inspector/IMG_20210617_124445.jpg",
      },
      {
        url: "http://localhost:5000/inspector/IMG_20210617_124509.jpg",
      },
      {
        url: "http://localhost:5000/inspector/IMG_20210617_124601.jpg",
      },
      {
        url: "http://localhost:5000/inspector/IMG_20210617_124620.jpg",
      },
    ],
  },
  {
    source: "cyclomedia",
    date: "12-05-2021",
    images: [
      {
        url: "http://localhost:5000/cyclomedia/IMG_20210617_142309.jpg",
      },
      {
        url: "http://localhost:5000/cyclomedia/IMG_20210617_142340.jpg",
      },
      {
        url: "http://localhost:5000/cyclomedia/IMG_20210617_142454.jpg",
      },
      {
        url: "http://localhost:5000/cyclomedia/IMG_20210617_142722.jpg",
      },
      {
        url: "http://localhost:5000/cyclomedia/IMG_20210617_142822.jpg",
      },
      {
        url: "http://localhost:5000/cyclomedia/IMG_20210617_142932.jpg",
      },
      {
        url: "http://localhost:5000/cyclomedia/IMG_20210617_143124.jpg",
      },
      {
        url: "http://localhost:5000/cyclomedia/IMG_20210617_152108.jpg",
      },
      {
        url: "http://localhost:5000/cyclomedia/IMG_20210617_152256.jpg",
      },
      {
        url: "http://localhost:5000/cyclomedia/IMG_20210617_152408.jpg",
      },
      {
        url: "http://localhost:5000/cyclomedia/IMG_20210617_152736.jpg",
      },
      {
        url: "http://localhost:5000/cyclomedia/IMG_20210617_152859.jpg",
      },
      {
        url: "http://localhost:5000/cyclomedia/IMG_20210617_152959.jpg",
      },
    ],
  },
];
