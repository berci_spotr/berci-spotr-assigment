/* eslint-disable */
const data = require("./data");

var path = require("path");
const express = require("express");
const cors = require("cors");

const app = express();

app.use(cors());
app.use(express.static(path.join(__dirname, "public")));

app.get("/images", (req, res) => {
  res.json(data);
});

app.listen(5000, () => console.log("server started"));
