import React, { StrictMode } from "react";
import ReactDOM from "react-dom";
import ImageViews from "./components/ImageViews";
import ErrorBoundary from "./components/ErrorBoundary";

// constant url that gets passed to the ImageViews component
const url = "http://localhost:5000/images";

export default function App(): JSX.Element {
  return (
    <>
      <ErrorBoundary>
        <ImageViews url={url} />
      </ErrorBoundary>
    </>
  );
}

ReactDOM.render(
  <StrictMode>
    <App />
  </StrictMode>,
  document.getElementById("root") as HTMLDivElement
);
