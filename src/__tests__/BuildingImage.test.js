/* eslint-disable */

import { expect } from "@jest/globals";
import { render, screen } from "@testing-library/react";
import BuildingImage from "../components/BuildingImage";

const data = {
  url: "http://localhost:5000/streetview/IMG_20210617_111603.jpg",
};

test("displays a loading fallback and assigns the correct source to img", () => {
  render(<BuildingImage element={data} />);
  const loadingFallback = screen.getByTestId("loadingFallback");
  expect(loadingFallback).toBeInTheDocument();
  const imageTag = screen.getByTestId("imageTag");
  expect(imageTag.src).toBe(
    "http://localhost:5000/streetview/IMG_20210617_111603.jpg"
  );
});
