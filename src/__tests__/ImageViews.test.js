/* eslint-disable */

import { expect } from "@jest/globals";
import { render, screen } from "@testing-library/react";
import ImageViews from "../components/ImageViews";

test("should render loading spinner", () => {
  const url = "http://localhost:5000/images";
  render(<ImageViews url={url} />);
  const loadingElement = screen.getByTestId("loading-testid");
  expect(loadingElement).toBeInTheDocument();
});
