/* eslint-disable */

import { expect } from "@jest/globals";
import { render, screen } from "@testing-library/react";
import PreviewImages from "../components/PreviewImages";
import ResizeObserver from "../__mocks__/ResizeObserverMock";

const data = {
  source: "streetview",
  date: "02-02-2021",
  images: [
    {
      url: "http://localhost:5000/streetview/IMG_20210617_111603.jpg",
    },
  ],
};

test("should render fallback animation", async () => {
  render(<PreviewImages data={data} handleTabChange={() => {}} />);
  expect(screen.getByTestId("loadingFallback")).toBeInTheDocument();
});

test("should render image with correct src", async () => {
  const previewImages = render(
    <PreviewImages data={data} handleTabChange={() => {}} />
  );
  const imageTag = await previewImages.findByTestId("imageTag");
  expect(imageTag).toBeInTheDocument();
  imageTag.src = data.images[0].url;
  expect(imageTag.src).toBe(
    "http://localhost:5000/streetview/IMG_20210617_111603.jpg"
  );
});
