import React, { useState } from "react";
import { HiArrowLeft } from "react-icons/hi";
import BuildingImage from "./BuildingImage";
import SourceInfo from "./SourceInfo";
import styles from "../styles/allImages.module.css";
import { ImageInterface, ResponseInterface } from "../interfaces/AppInterfaces";
import { FaAngleLeft, FaAngleRight } from "react-icons/fa";

export default function CompleteImageOverview({
  data,
  handleTabChange,
}: {
  data: ResponseInterface | null;
  handleTabChange: (data: ResponseInterface | null) => void;
}): JSX.Element {
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [images, setImages] = useState<ImageInterface[] | undefined>(
    data?.images.slice(0, 10)
  );
  const [nextPage, setNextPage] = useState<ImageInterface[] | undefined>(
    data?.images.slice(10)
  );
  const [prevPage, setPrevPage] = useState<ImageInterface[] | undefined>(
    undefined
  );

  // renders empty fallback if no data is provided
  if (!data || !images) return <></>;

  // alters the state on backwards button click
  function handlePrevPage(): void {
    setCurrentPage(currentPage - 1);
    setImages(prevPage);
    setNextPage(images);
    setPrevPage(images?.slice(currentPage * 10, 9));
  }

  // alters the state on forward button click
  function handleNextPage(): void {
    setCurrentPage(currentPage + 1);
    setImages(nextPage);
    setPrevPage(images);
    setNextPage(nextPage?.slice(10));
  }

  // navigation component taken out of the render statement for readability
  const Navigation = (
    <div className={styles.NavigationWrapper}>
      <button
        type="button"
        disabled={prevPage?.length === 0 || prevPage === undefined}
        onClick={() => {
          handlePrevPage();
        }}
      >
        <FaAngleLeft />
      </button>
      <span>
        Page {currentPage} of {Math.ceil(data.images.length / 10)}
      </span>
      <button
        type="button"
        disabled={nextPage?.length === 0}
        onClick={() => {
          handleNextPage();
        }}
      >
        <FaAngleRight />
      </button>
    </div>
  );

  return (
    <>
      <div className={styles.SourceInfoWrapper}>
        <button onClick={() => handleTabChange(null)}>
          <HiArrowLeft />
        </button>
        <SourceInfo source={data.source} date={data.date} />
      </div>
      <div className={styles.ImagesGrid}>
        {images.map((image) => (
          <BuildingImage key={image.url} element={image} />
        ))}
      </div>
      {Navigation}
    </>
  );
}
