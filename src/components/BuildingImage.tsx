import React, { useState } from "react";
import styles from "../styles/buildingImage.module.css";
import { ImageInterface } from "../interfaces/AppInterfaces";

export default function BuildingImage({
  element,
}: {
  element: ImageInterface;
}): JSX.Element {
  // checks if the image is loaded, while false it displays a loading fallback
  const [isLoaded, setIsLoaded] = useState<boolean>(false);

  return (
    <a
      href={element.url}
      target="_blank"
      rel="noreferrer"
      key={element.url}
      className={styles.ImageContainer}
    >
      <div
        className={styles.PlaceHolder}
        style={{ visibility: isLoaded ? "hidden" : "visible" }}
        data-testid="loadingFallback"
      >
        <div className={styles.PlaceHolderHelper} />
      </div>
      <img
        data-testid="imageTag"
        src={element.url}
        alt="preview"
        className={styles.Image}
        onLoad={() => setIsLoaded(true)}
      />
    </a>
  );
}
