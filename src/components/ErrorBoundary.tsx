import React, { Component, ErrorInfo, ReactNode } from "react";
import styles from "../styles/errorBoundary.module.css";

export default class ErrorBoundary extends Component {
  state = { isError: false, message: "" };

  // catches errors that happen in the components synchronously
  // and modifies the state
  componentDidCatch(error: Error, info: ErrorInfo): void {
    this.setState({ isError: true, message: error.message });
    console.error("ErrorBoundary caught an error", error, info);
  }

  // reloads the page, used on a button in the error message shown to the user
  reloadPage = (): void => {
    location.reload();
  };

  // when an error is caught, the children of this component become the JSX below
  // otherwise, children render as usual
  render(): ReactNode {
    if (this.state.isError) {
      return (
        <div className={styles.ErrorContainer}>
          <span role="img" aria-label="errorsign">
            🚧
          </span>
          <h2>An error has occurred.</h2>
          <h4>{this.state.message}</h4>
          <p>
            Please come back later, or click{" "}
            <button type="button" onClick={() => this.reloadPage()}>
              here
            </button>{" "}
            to reload the page.
          </p>
        </div>
      );
    }

    return this.props.children;
  }
}
