import React, { useState, useEffect } from "react";
import PreviewImages from "./PreviewImages";
import AllImages from "./AllImages";
import styles from "../styles/imageViews.module.css";
import { ResponseInterface } from "../interfaces/AppInterfaces";
import LoadingSpinner from "./LoadingSpinner";

const initialState: ResponseInterface[] = [
  {
    source: " ",
    date: " ",
    images: [{ url: "" }],
  },
];

export default function ImagesOverview({ url }: { url: string }): JSX.Element {
  const [sources, setSources] = useState<ResponseInterface[]>(initialState);
  const [loading, setLoading] = useState<boolean>(true);
  const [isPreview, setIsPreview] = useState<boolean>(true);
  const [currentSourceData, setCurrentSourceData] =
    useState<ResponseInterface | null>(initialState[0]);
  const [error, setError] = useState({
    status: false,
    message: "",
    statusCode: 200,
  });

  // custom error handling, ErrorBoundary does not catch asynchronous errors
  // so we have to throw it manually
  if (error.status)
    throw new Error(String(error.statusCode) + " " + error.message);

  useEffect(() => {
    const getJSON = async () => {
      const res = await fetch(url);
      if (!res.ok) {
        setError({
          status: true,
          message: res.statusText,
          statusCode: res.status,
        });
      }
      const data = (await res.json()) as ResponseInterface[];
      setSources(data);
      setLoading(false);
    };
    void getJSON();
  }, [url]);

  // handles the change between the PreviewImages and AllImages component
  // and passes the respective dataset to the AllImages component
  function handleTabChange(data: ResponseInterface | null): void {
    setIsPreview(!isPreview);
    setCurrentSourceData(data);
  }

  // renders a loading spinner on first load
  // and while the API call does not finish
  if (loading)
    return (
      <div className={styles.SpinnerWrapper} data-testid="loading-testid">
        <LoadingSpinner />
      </div>
    );

  // creates a list of PreviewImages, taken out of the return statement
  // for better readability and easier conditional rendering based on isPreview
  const PreviewList = sources.map((s) => (
    <PreviewImages key={s.source} data={s} handleTabChange={handleTabChange} />
  ));

  return (
    <div className={styles.ImageViews} data-testid="imageviews-testid">
      {isPreview ? (
        PreviewList
      ) : (
        <AllImages handleTabChange={handleTabChange} data={currentSourceData} />
      )}
    </div>
  );
}
