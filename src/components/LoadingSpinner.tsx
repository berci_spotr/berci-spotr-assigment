import React from "react";
import styles from "../styles/loadingSpinner.module.css";

export default function LoadingSpinner(): JSX.Element {
  return (
    <div className={styles.LdsRing}>
      <div />
      <div />
      <div />
      <div />
    </div>
  );
}
