import React, { useState, useEffect, useRef } from "react";
import BuildingImage from "./BuildingImage";
import SourceInfo from "./SourceInfo";
import styles from "../styles/previewImages.module.css";
import useContainerSize from "../hooks/useContainerSize";
import { HiArrowRight } from "react-icons/hi";
import {
  ResponseInterface,
  ImageInterface,
  ContainerInterface,
} from "../interfaces/AppInterfaces";

const initialState: ImageInterface[] = [{ url: "" }];

export default function SourceImages({
  data,
  handleTabChange,
}: {
  data: ResponseInterface;
  handleTabChange: (data: ResponseInterface | null) => void;
}): JSX.Element {
  const [previews, setPreviews] = useState<ImageInterface[]>(initialState);

  // creates a ref that can be used by the useContainerSize custom hook
  // to measure the parent component's width
  const ref = useRef<HTMLDivElement>(null);

  const size: ContainerInterface = useContainerSize(ref);

  useEffect(() => {
    // every time the container size changes, it checks how many images should be rendered
    // it does so without having to modify the original array
    function handleArraySlice(): void {
      if (size.width && size.width >= 1920) {
        setPreviews(data.images.slice(0, 6));
      } else if (size.width && size.width >= 1440) {
        setPreviews(data.images.slice(0, 5));
      } else if (size.width && size.width >= 1024) {
        setPreviews(data.images.slice(0, 4));
      } else if (size.width && size.width >= 769) {
        setPreviews(data.images.slice(0, 3));
      } else if (size.width && size.width >= 481) {
        setPreviews(data.images.slice(0, 2));
      } else if (size.width && size.width <= 480) {
        setPreviews(data.images.slice(0, 1));
      }
    }

    handleArraySlice();
  }, [size.width, data.images]);

  return (
    <div className={styles.SourceImages} ref={ref}>
      <div className={styles.ImagesInfo}>
        <SourceInfo source={data.source} date={data.date} />
        <button
          type="button"
          className={styles.AllImagesButton}
          onClick={() => handleTabChange(data)}
        >
          <span>All {data.images.length} images</span>
          <HiArrowRight className={styles.RightArrow} />
        </button>
      </div>
      <div className={styles.ImageGrid}>
        {previews.map(function createGrid(element) {
          return <BuildingImage element={element} key={element.url} />;
        })}
      </div>
    </div>
  );
}
