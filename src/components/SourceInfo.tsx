import React from "react";
import styles from "../styles/sourceInfo.module.css";
import { FaImages, FaGoogle, FaUserPlus, FaCodiepie } from "react-icons/fa";

export default function SourceInfo({
  source,
  date,
}: {
  source: string;
  date: string;
}): JSX.Element {
  // assigns the correct icon to each source
  // if there's no match then it renders a fallback <FaImages /> component
  let icon = <FaImages />;
  if (source === "streetview") {
    icon = <FaGoogle />;
  } else if (source === "inspector") {
    icon = <FaUserPlus />;
  } else if (source === "cyclomedia") {
    icon = <FaCodiepie />;
  }

  return (
    <div className={styles.SourceInfo}>
      <div className={styles.SourceIconWrapper}>{icon}</div>
      <div className={styles.SourceTextWrapper}>
        <h4>{source[0].toUpperCase() + source.slice(1)}</h4>
        <div className={styles.Divider} />
        <h4>{date}</h4>
      </div>
    </div>
  );
}
