import { useState, useEffect } from "react";
import { ContainerInterface } from "../interfaces/AppInterfaces";

export default function useContainerSize(
  ref: React.RefObject<HTMLDivElement>
): ContainerInterface {
  const [containerSize, setContainerSize] = useState<ContainerInterface>({
    width: undefined,
  });

  // assigns a ResizeObserver to the passed in element
  // and sets the state based on the width of that element on change
  // it removes the observer after the state update
  // making sure that it only runs while the screen is changing
  useEffect(() => {
    const resize_ref = new ResizeObserver((entries) => {
      const width = entries[0].contentRect.width;
      setContainerSize({ width: width });
    });

    const refCopy = ref.current;

    resize_ref.observe(refCopy as Element);

    return () => resize_ref.unobserve(refCopy as Element);
  }, [ref]);

  return containerSize;
}
