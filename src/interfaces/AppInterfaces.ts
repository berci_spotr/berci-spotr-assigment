export interface ImageInterface {
  url: string;
}

export interface ResponseInterface {
  source: string;
  date: string;
  images: ImageInterface[];
}

export interface ContainerInterface {
  width: number | undefined;
}
